var mainImg = document.getElementById("mainImg");
var topImg = document.getElementById("topImg");
var leftImg = document.getElementById("leftImg");
var rightImg = document.getElementById("rightImg");
var bottomImg = document.getElementById("bottomImg");


// Touch to begin jquery 

function touchToStart() {
  if (mainImg.className == "mainImg") {
    $(".mainImg").attr("src", "./img/main-circle-active.png");
    $(".leftImg").removeClass(" hide");
    $(".topImg").removeClass(" hide");
    $(".bottomImg").removeClass(" hide");
    $(".rightImg").removeClass(" hide");
    $(".mainImg").addClass(" active");
    $(".topImg").css({
      "display": "block"
    });
    $(".leftImg").css({
      "display": "block"
    });
    $(".rightImg").css({
      "display": "block"
    });
    $(".bottomImg").css({
      "display": "block"
    });
  } else {
    $(".mainImg").attr("src", "./img/main-circle-inactive.png");
    $(".leftImg").addClass(" hide");
    $(".topImg").addClass(" hide");
    $(".bottomImg").addClass(" hide");
    $(".rightImg").addClass(" hide");
    $(".mainImg").removeClass(" active");
    $(".topVideo").addClass(" hide");
    $(".leftVideo").addClass(" hide");
    $(".rightVideo").addClass(" hide");
    $(".bottomVideo").addClass(" hide");
  }
}

function touchTop() {

  $(".topImg").addClass(" hide");
  $(".topImg").addClass(" active");
  $(".topVideo").removeClass(" hide")
  $(".topImg").css({
    "display": "none"
  });
  $(".topVideo").css({
    "display": "block"
  });
  $(".leftVideo").css({
    "display": "none"
  });
  $(".rightVideo").css({
    "display": "none"
  });
  $(".bottomVideo").css({
    "display": "none"
  });
  $(".leftImg").css({
    "display": "block"
  });
  $(".rightImg").css({
    "display": "block"
  });
  $(".bottomImg").css({
    "display": "block"
  });
  $(".leftImg").removeClass(" hide")
  $(".rightImg").removeClass(" hide")
  $(".bottomImg").removeClass(" hide")
  
document.getElementById("topVideo").load();
}

function touchTopVideo() {
  $(".topImg").removeClass(" hide");
  $(".topImg").removeClass(" active");
  $(".topImg").css({
    "display": "block"
  });
  $(".topVideo").css({
    "display": "none"
  });
}

function touchLeft() {
  $(".leftImg").addClass(" hide");
  $(".leftImg").addClass(" active");
  $(".leftVideo").removeClass(" hide")
  $(".leftImg").css({
    "display": "none"
  });
  $(".leftVideo").css({
    "display": "block"
  });
  $(".topVideo").css({
    "display": "none"
  });
  $(".rightVideo").css({
    "display": "none"
  });
  $(".bottomVideo").css({
    "display": "none"
  });
  $(".topImg").css({
    "display": "block"
  });
  $(".rightImg").css({
    "display": "block"
  });
  $(".bottomImg").css({
    "display": "block"
  });
  $(".topImg").removeClass(" hide")
  $(".rightImg").removeClass(" hide")
  $(".bottomImg").removeClass(" hide")
  
document.getElementById("leftVideo").load();
}

function touchLeftVideo() {
  $(".leftImg").removeClass(" hide");
  $(".leftImg").removeClass(" active");
  $(".leftImg").css({
    "display": "block"
  });
  $(".leftVideo").css({
    "display": "none"
  });
}

function touchRight() {
  $(".rightImg").addClass(" hide");
  $(".rightImg").addClass(" active");
  $(".rightVideo").removeClass(" hide")
  $(".rightImg").css({
    "display": "none"
  });
  $(".rightVideo").css({
    "display": "block"
  });
  $(".leftVideo").css({
    "display": "none"
  });
  $(".topVideo").css({
    "display": "none"
  });
  $(".bottomVideo").css({
    "display": "none"
  });
  $(".leftImg").css({
    "display": "block"
  });
  $(".topImg").css({
    "display": "block"
  });
  $(".bottomImg").css({
    "display": "block"
  });
  $(".leftImg").removeClass(" hide")
  $(".topImg").removeClass(" hide")
  $(".bottomImg").removeClass(" hide")
  
document.getElementById("rightVideo").load();
}

function touchRightVideo() {
  $(".rightImg").removeClass(" hide");
  $(".rightImg").removeClass(" active");
  $(".rightImg").css({
    "display": "block"
  });
  $(".rightVideo").css({
    "display": "none"
  });
}

function touchBottom() {
  $(".bottomImg").addClass(" hide");
  $(".bottomImg").addClass(" active");
  $(".bottomVideo").removeClass(" hide")
  $(".bottomImg").css({
    "display": "none"
  });
  $(".bottomVideo").css({
    "display": "block"
  });
  $(".leftVideo").css({
    "display": "none"
  });
  $(".rightVideo").css({
    "display": "none"
  });
  $(".topVideo").css({
    "display": "none"
  });
  $(".leftImg").css({
    "display": "block"
  });
  $(".rightImg").css({
    "display": "block"
  });
  $(".topImg").css({
    "display": "block"
  });
  $(".leftImg").removeClass(" hide")
  $(".rightImg").removeClass(" hide")
  $(".topImg").removeClass(" hide")
  
document.getElementById("bottomVideo").load();
}

function touchBottomVideo() {
  $(".bottomImg").removeClass(" hide");
  $(".bottomImg").removeClass(" active");
  $(".bottomImg").css({
    "display": "block"
  });
  $(".bottomVideo").css({
    "display": "none"
  });
}


//=================== INTERACT JS ===================

//======================OBJ1=========================

interact('.drag-drop')
  .draggable({
    inertia: true,
    modifiers: [
      interact.modifiers.restrict({
        restriction: "parent",
        endOnly: true,
        elementRect: {
          top: 0,
          left: 0,
          bottom: 1,
          right: 1
        }
      })
    ],
    autoScroll: true,
    onmove: dragMoveListener
  });

function dragMoveListener(event) {

  var target = event.target,
    // keep the dragged position in the data-x/data-y attributes

    x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
    y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

  // translate the element
  target.style.webkitTransform =
    target.style.transform =
    'translate(' + x + 'px, ' + y + 'px)';

  // update the posiion attributes
  target.setAttribute('data-x', x);
  target.setAttribute('data-y', y);
};